<?php
require_once 'phpQuery-onefile.php';	//html parsing library 

/*
	Class: ScraperClass
	Author: ngaziz
	Date: 12.06.2016
	Description:
	Scraping events details from one category of residentadvisor.net
	Return results in json
*/
class ScraperClass
{
	var $baseUrl;		//base website url
	var $mainPageUrl;	//list of events url
	var $eventsUrl;	    //urls of each event page
	var $events;		//scraping results
	var $logsOn;		//print logs (true, false)
	
	/*
		Constructor
		$main_page_url - list of events url
		$logs = true - print logs
	*/
	function ScraperClass($main_page_url,$logs)
	{	
		$this->logsOn=$logs;
		$this->baseUrl="https://www.residentadvisor.net/";
		$this->Log("start scraping ".$this->baseUrl);
		$this->mainPageUrl=$this->baseUrl.$main_page_url;
		$this->ParseMainPage();							//get events list from main page
		foreach ($this->eventsUrls as $url)
		{
			$this->ParseEventPage($url);				//get event details
		}
		$this->Log("events data: ");
		$json=json_encode($this->events);				//output results in json
		echo $json."<br>";
		file_put_contents('results.json',$json);		//save results into file
		$this->Log("finish scraping");
	}
	
	/*
		Print log message
	*/
	function Log($message)
	{
		if ($this->logsOn) echo @date("H:i:m").": ".$message."\r\n<br>";
	}
	
	/*
		Get html page using cURL
	*/
	function GetPage($url)
	{
		$options = array(
			CURLOPT_USERAGENT => 'Mozilla/5.0 (compatible)',
			//CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => false,
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_CONNECTTIMEOUT => 5,
			CURLOPT_TIMEOUT => 5
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt_array($ch, $options);
		$html = curl_exec($ch);
		curl_close($ch);
		return $html;
	}

	/*
		Get all event urls from main page
	*/
	function ParseMainPage()
	{
		$html=$this->GetPage($this->mainPageUrl);
		$this->Log("parse main page ".$this->mainPageUrl);
		//file_put_contents('main_page.html',$html);
		try
		{
			$results = phpQuery::newDocument($html);	//using phpQuery for html document parsing
		}
		catch (Exception $e)
		{
			$this->Log("phpQuery error: ".$e);
			return false;
		}
		$this->eventsUrls=array();
		$events = $results->find("ul.list.small.eventsList > li > article");
		foreach ($events as $event)
		{
			$this->eventsUrls[]=$this->baseUrl.pq($event)->find("a:first")->attr("href");
		}		
		//var_dump($this->eventsUrls);
		$this->Log(count($this->eventsUrls)." event urls founded");
	}
	
	/*
		Remove &nbsp from string
	*/
	function RemoveSpecSym($text)
	{
		$text = htmlentities($text);
		$text = str_replace("&nbsp;",'',$text);
		$text = html_entity_decode($text);
		return trim($text);
	}
	
	/*
		Get event details from url
	*/
	function ParseEventPage($url)
	{
		$html=$this->GetPage($url);
		$this->Log("parse event page ".$url);
		try
		{
			$results = phpQuery::newDocument($html);
		}
		catch (Exception $e)
		{
			$this->Log("phpQuery error: ".$e);
			return false;
		}
		$event = array();

		$event['title'] = $results->find("div#sectionHead > h1")->text();
        $event['date'] = $results->find("ul.clearfix > li:eq(0)")->text();
        $event['location'] = $results->find("ul.clearfix > li:eq(1)")->text();
        $event['price'] = $results->find("ul.clearfix > li:eq(2)")->text();
        $event['image'] = $results->find("div.flyer > a")->attr("href");
        
		//var_dump($event);
        
		$this->events[]=$event;
	}

}

?>